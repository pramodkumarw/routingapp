import { Component, OnInit, ViewChild ,ElementRef, AfterViewInit} from '@angular/core';
import { StateService } from "../services/state.service";


@Component({
  selector: 'app-electronic',
  templateUrl: './electronic.component.html',
  styles: []
})
export class ElectronicComponent implements OnInit, AfterViewInit
 {

  constructor(private state:StateService) { }

  @ViewChild('name') test:ElementRef;
  ngOnInit() {
    this.name=this.state.name; 

    }
 
  set name(value:string){
      this.state.name=value;
  }

  get name() :string{
    return this.state.name; 
  }

  valuechange(e){
    this.name=e.value;
 
  }
  
  ngAfterViewInit(){
    this.test.nativeElement.value=this.state.name;
  }

}
