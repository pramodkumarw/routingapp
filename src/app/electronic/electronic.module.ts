import { NgModule } from '@angular/core';
import { ElectronicComponent } from "./electronic.component";
import {  Routes,RouterModule} from "@angular/router";
import { ElectronicDetailComponent } from '../electronic-detail/electronic-detail.component';
import { ElectronicEditComponent } from '../electronic-edit/electronic-edit.component';

const routes:Routes=[
  { 
    path:"",
    component:ElectronicComponent
  },
  {
    path:":id/edit",
    component:ElectronicEditComponent
  },
  {
    path:":id",
    component:ElectronicDetailComponent
  }
]
@NgModule({
  imports: [    
    RouterModule.forChild(routes)
  ],
  declarations: [
    ElectronicComponent,
    ElectronicDetailComponent,
    ElectronicEditComponent
  ]
})
export class ElectronicModule { }
