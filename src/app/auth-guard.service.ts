import { Injectable } from '@angular/core';

import { Route,CanLoad } from "@angular/router"

@Injectable()
export class AuthGuard implements  CanLoad {

    canLoad(route: Route): boolean {
        return true;
    }

}
